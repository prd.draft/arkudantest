﻿using System.IO;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Text.Json;

namespace D1
{
    public class Program
    {

        private static Config config = new Config()
        {
            repo_id = "37985107",
            branch_name = "report-upload",
            private_key = "glpat-wyKs2jjKigqyo76A9aqr",
        };
        static void Main(string[] args)
        {

            MainAsync(args).GetAwaiter().GetResult();
        }

        static async Task MainAsync(string[] args)
        {


            if (args.Length == 1)
            {

                if (File.Exists("config.json"))
                {
                    var configString = await File.ReadAllTextAsync("config.json");
                    var externalConfig = JsonSerializer.Deserialize<Config>(configString);
                    if (externalConfig != null)
                    {
                        config = externalConfig;
                    }
                }
                else
                {
                    var configString = JsonSerializer.Serialize(config);
                    await File.WriteAllTextAsync("config.json", configString);
                }

                if (string.IsNullOrEmpty(config.repo_id))
                {
                    Console.WriteLine("repo_id is empty. please set it in config.json or remove config.json for using default value");
                    Console.ReadKey();
                    return;
                }

                if (string.IsNullOrEmpty(config.branch_name))
                {
                    Console.WriteLine("branch_name is empty. please set it in config.json or remove config.json for using default value");
                    Console.ReadKey();
                    return;
                }

                if (string.IsNullOrEmpty(config.private_key))
                {
                    Console.WriteLine("private_key is empty. please set it in config.json or remove config.json for using default value");
                    Console.ReadKey();
                    return;
                }

                var filePath = args[0];
                FileInfo fileInfo = new FileInfo(filePath);

                var project_code = fileInfo.Name.Substring(0, 4);


                var bytes = await File.ReadAllBytesAsync(filePath);
                var base64String = Convert.ToBase64String(bytes);
                var file_path = $"Reports/{project_code}/{fileInfo.Name}";


                var fileExist = await FileIsExistOnGitlab(config.private_key, file_path, config.repo_id, config.branch_name);
                var action = fileExist ? "update" : "create";
                Console.WriteLine($">Project Code: {project_code}");
                Console.WriteLine($">File Name: {fileInfo.Name}");
                Console.WriteLine(">Enter Test Run ID:");
                var run_id = Console.ReadLine();
                Console.WriteLine(">Pushing file...");
                Console.WriteLine(">action:" + action);

                var pushTask = PushFileToGitlab(config.private_key, action, $"{project_code} {run_id}", file_path, config.repo_id, config.branch_name, base64String);
                var response = await pushTask;
                if (pushTask.IsFaulted)
                {
                    Console.WriteLine(">Fail to push file");
                }
                else
                {
                     await File.WriteAllTextAsync("log.txt", response);
                    Console.WriteLine(">Push file successfully !!!, Plase any key to exit");
                    try
                    {
                        var url = $"https://gitlab.com/proudia-studio-unity/development/d1/qa-tool/-/tree/report-upload/Reports/{project_code}";
                        Process.Start(new ProcessStartInfo { FileName = url, UseShellExecute = true });
                    }
                    catch
                    {
                        throw;
                    }
                }

                Console.ReadKey();

            }
        }

        private static async Task<bool> FileIsExistOnGitlab(string private_key, string filePath, string repo_id, string branch)
        {
            var api = $"https://gitlab.com/api/v4/projects/{repo_id}/repository/files/{filePath}?ref={branch}";
          //  Console.WriteLine($">Checking file exist on gitlab: {api}");
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("PRIVATE-TOKEN", private_key);

            var response = await httpClient.GetAsync(api);

         //   Console.WriteLine($">Response status code: {response.StatusCode}");
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static async Task<string> PushFileToGitlab(string private_key, string action, string commit_message, string filePath, string repo_id, string branch, string data)
        {
            var api = $"https://gitlab.com/api/v4/projects/{repo_id}/repository/commits";

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("PRIVATE-TOKEN", private_key);

            var gitlabAction = new GitLabAction()
            {
                action = action,
                file_path = filePath,
                content = data,
                encoding = "base64"
            };

            var gitlabPost = new GitLabPost()
            {
                branch = branch,
                commit_message = commit_message,
                actions = new GitLabAction[1] { gitlabAction }
            };

            string jsonString = JsonSerializer.Serialize(gitlabPost);
            var response = await httpClient.PostAsync(api, new StringContent(jsonString, Encoding.UTF8, "application/json"));
            return await response.Content.ReadAsStringAsync();

        }

    }
    [Serializable]
    public struct GitLabPost
    {
        public string branch { get; set; }
        public string commit_message { get; set; }
        public GitLabAction[] actions { get; set; }
    }

    [Serializable]
    public struct GitLabAction
    {
        public string action { get; set; }
        public string file_path { get; set; }
        public string content { get; set; }
        public string encoding { get; set; }

    }

    [Serializable]
    public class Config
    {
        public string? repo_id { get; set; }
        public string? branch_name { get; set; }
        public string? private_key { get; set; }
    }

}
